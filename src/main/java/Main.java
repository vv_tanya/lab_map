import java.util.*;

public class Main {

    public static Map swap(Map<String, String> map) {
        Map<String, Collection<String>> mapEnd = new HashMap<>();
        Set<String> keySet = map.keySet();
        for(String key : keySet){
            String[] values = map.get(key).toString().split(", ");
            for (String value:values) {
                mapEnd.compute(value, (v, k) -> {
                    if (k == null) {
                        k = new HashSet<>();
                    }
                    k.add(String.valueOf(key));
                    return k;
                });
            }
        }
        return mapEnd;
    }

    public static void main(String[] args){
        Map<String, String> map = new HashMap<String, String>();

        String key [] = {"1", "2", "1", "4", "5", "6", "4"};
        String value [] = {"a", "b", "c", "a", "d", "c", "b"};

        int count = 0;
        for (String keys : key ){
            if (map.get(keys) == null) {
                map.put(keys, value[count]);
            }
            else {
                int finalCount = count;
                map.compute(keys, (k, v) ->
                        v + ", " + value[finalCount]);
            }
            count++;
        }

        System.out.println("До перестановки:");
        for(Map.Entry en:map.entrySet())
            System.out.println(en.getKey() + " " + en.getValue());

        map=swap(map);

        System.out.println("После перестановки:");
        for(Map.Entry en:map.entrySet())
            System.out.println(en.getKey() + " " + en.getValue().toString().replaceAll("^\\[|\\]$", ""));
    }
}